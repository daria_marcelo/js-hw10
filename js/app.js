let ul = document.querySelector(".tabs");
ul.addEventListener("click", function (ev) {
    console.log(ev.target.dataset.tab);
    let data = ev.target.dataset.tab;

    console.log(document.querySelector(".active-description"));
    document.querySelector(".active-description").classList.remove("active-description");

    console.log(document.querySelector(".active-tab"));
    document.querySelector(".active-tab").classList.remove("active-tab");

    console.log(document.querySelector(`[data-li = ${data}]`));
    document.querySelector(`[data-li = ${data}]`).classList.add("active-description");

    ev.target.classList.add("active-tab");
});
